"""Personal_Website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from first_version.views import home
from first_version.views import feedback
from first_version.views import register
from first_version.views import activity
from first_version.views import schedule
from first_version.views import saveactivity
from first_version.views import display_schedule
from first_version.views import success
from first_version.views import delete_schedule




urlpatterns = [
    path('admin/', admin.site.urls),
    # path('feedback/', feedback),
    # path('home/', home),
    # path('register/', register),
    # path('activity/',activity),
    # path('schedule/',schedule),
    # path('save_activity/',saveactivity),
    # path('display_schedule/',display_schedule),
    # path('success/',success),
    path('',include(('first_version.urls','first_version'),namespace='first_version')),
    # path('schedule_delete/',delete_schedule),
   # path('',include('first_version.urls'))
]
