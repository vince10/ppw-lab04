from django.apps import AppConfig


class FirstVersionConfig(AppConfig):
    name = 'first_version'
