from django import forms

class Activity_Form(forms.Form):
   
    error_messages = {
        'required': 'Please type',
    }
    activity_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder': 'activity'
        
    }
    place_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'place'
    }
    category_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'category'
    }
    date_attrs={
        'type':'date'
        
    }

    category_choices=(
        ('indoor','indoor'),
        ('outdoor','outdoor'),
        ('a bit of both','a bit of both'),
    )

    activity = forms.CharField(label='Activity', required=True, max_length=27, widget=forms.TextInput(attrs=activity_attrs))
    place = forms.CharField(label='Place', required=True, max_length=27, widget=forms.TextInput(attrs=place_attrs))
    #category = forms.CharField(label='Category', required=True,max_length=27,choices=category_choices)
    category = forms.ChoiceField(label='Category', required=True,choices=category_choices)
    #date = forms.CharField(label='Date', required=True, max_length=27, widget=forms.DateInput(attrs=date_attrs))
    date = forms.DateField(label='Date',required=True,widget=forms.DateInput(attrs=date_attrs))
    time = forms.TimeField(label='Time',required=True,widget=forms.TimeInput(attrs={'type':'time'}))
