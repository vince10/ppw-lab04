from django.db import models
from django.utils import timezone

class Activity(models.Model):
   
    activity = models.CharField(max_length=200)
    place = models.CharField(max_length=200)
    category = models.CharField(max_length=200)
    date = models.DateField()
    time = models.TimeField()
    created_date = models.DateTimeField(default=timezone.now)



# Create your models here.
