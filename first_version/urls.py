from django.urls import re_path
from django.urls import path
from .views import home,feedback,register,activity,schedule,saveactivity,display_schedule,success,delete_schedule
#url for app
 
 #path('',home, name='home'),
  #  path('feedback/',feedback, name='feedback'),
   # path('register/',register, name='register'),
    #path('activity/',activity,name='activity'),
    #re_path(r'^$',home, name='home'),
    #re_path(r'^feedback/$',feedback, name='feedback'),
    #re_path(r'^register/$',register, name='register'),
    #re_path(r'^activity/$',activity,name='activity'),
urlpatterns = [
   path('',home, name='home'),
    path('feedback/',feedback, name='feedback'),
    path('register/',register, name='register'),
    path('activity/',activity,name='activity'),
    path('schedule/',schedule,name='schedule'),
     path('save_activity/',saveactivity,name='saveactivity'),
      path('display_schedule/',display_schedule,name='display_schedule'),
      path('success/',success,name='success'),
      path('schedule_delete/',delete_schedule,name='delete_schedule')


 
    
  
]
   