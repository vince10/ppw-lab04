from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Activity_Form
from .models import Activity

response={}

def home(request):
    #response={}
    return render(request,'homepage.html',response)
def feedback(request):
    #response={}
    return render(request,'feedback.html',response)
def register(request):
    #response={}
    return render(request,'Form_Register_Account.html',response)
def schedule(request):
    return render(request,'schedule.html',response)
def success(request):
    return render(request,'success.html',response)
def activity(request):
    response['title'] = 'Add Activity'
    response['add_activity_form'] =  Activity_Form()
    return render(request,'activity.html',response)
def saveactivity(request):
    form = Activity_Form(request.POST or None)
    if(request.method=='POST' and form.is_valid()):
        response['activity']= request.POST['activity']
        response['place']=request.POST['place']
        response['category']=request.POST['category']
        response['date'] = request.POST['date']
        response['time'] = request.POST['time']

        Activities= Activity(
            activity=response['activity'],
            place=response['place'],
            category=response['category'],
            date=response['date'],
            time=response['time']
        )
        Activities.save()
        return render(request,'success.html',response)
    else:
        return HttpResponseRedirect('/')
def display_schedule(request):
    all_schedules = Activity.objects.all()
    response['all_schedules'] = all_schedules
    return render(request,'schedule.html',response)
def delete_schedule(request):
    print("1")
    Activity.objects.all().delete()
    print("2")
    return HttpResponseRedirect('/display_schedule')





# Create your views here.
